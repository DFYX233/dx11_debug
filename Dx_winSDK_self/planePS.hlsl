#include "Triangle.hlsli"


float4 main(VertexPosHWNormalTex i) :SV_Target
{


    float3 normal = normalize(i.normalW);
    float3 lightDir = float3(0.2,-1.0f,0.2f);
    float diff = max(0,dot(normal, -lightDir)) * 0.5f + 0.5f;


    float3 finalColor = diff * float3(0.7f,0.2f,0.0f);


    return float4(finalColor,1.0f);
}