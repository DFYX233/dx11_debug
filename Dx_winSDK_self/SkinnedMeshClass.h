#pragma once

#ifndef _SKINNED_MODEL_CLASS
#define _SKINNED_MODEL_CLASS
#include"MeshGeometryClass.h"
#include"SkinnedDataClass.h"
#include<D3Dcommon.h>  


class SkinnedModelClass
{

public:
	UINT SubsetCount;
	vector<ID3D11ShaderResourceView*> DiffuseMapSRV; //漫反射
	vector<ID3D11ShaderResourceView*> NormalMapSRV; //法线贴图


	vector<unsigned long> Indices;
	SkinnedDataClass SkinnedData;
	MeshGeometry ModelMesh;

public:
	SkinnedModelClass(ID3D11Device* device, const string& modelFileName);
	~SkinnedModelClass();


};

struct SkinnedModelInstance
{
	SkinnedModelClass* Model;
	float TimePos;
	std::string ClipName;
	XMFLOAT4X4 World;
	std::vector<XMFLOAT4X4> FinalTransforms;

	void Update(float dt);
};

#endif