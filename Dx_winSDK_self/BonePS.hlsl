#include "Triangle.hlsli"


float4 main(VertexPosHWNormalTex i) :SV_Target
{

    float3 normal = normalize(i.normalW);
    float3 lightDir = float3(0.2,-1.0f,0.2f);
    float diff = max(0,dot(normal, -lightDir)) * 0.5f + 0.5f;

    //float3 tempColor = texAnimation.Sample(gSampler, i.uv);
    //float3 finalColor = diff * float3(0.4f, 0.8f, 0.2f);
    float3 finalColor = diff * i.color  ;


    return float4(finalColor*2,1.0f);
}