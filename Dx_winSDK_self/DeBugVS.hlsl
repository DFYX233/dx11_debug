#include "Triangle.hlsli"

VertexPosHWNormalTex VShader(SkinnedVertexIn v)
{
    VertexPosHWNormalTex o;
    o.pos = mul(float4(v.pos, 1.0f), world);
    o.pos = mul(o.pos, view);
    o.pos = mul(o.pos, proj);
    return o;
}
