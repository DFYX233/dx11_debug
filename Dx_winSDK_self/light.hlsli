struct DirectLight
{
	float3 dir;
	float inti;
};
struct PointLight
{
	float3 pos;
	float range;

	float3 color;
};

struct SpotLight
{
	float3 pos;
	float range;
	float3 dir;
	float spot;
};

