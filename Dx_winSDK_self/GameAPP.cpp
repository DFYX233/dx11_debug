
#include <d3dcompiler.h>
#include "DXTrace.h"
#include "dxUtil.h"
#include <DirectXTex.h>
#include "GameApp.h"
#include "d3dAPP.h"
#include "MeshGeometryClass.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <random>

#define IMAGE_HEIGHT 512
#define IMAGE_WIDTH 512
#define INSTANCE_AMOUNT 100
using namespace DirectX;
struct VertexPosColor
{
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT2 uv;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};
struct VertexPosNormalTex
{
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 normal;
    DirectX::XMFLOAT2 uv;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};
struct SkinnedVertexIn
{
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 normal;
    DirectX::XMFLOAT2 uv;
    DirectX::XMFLOAT4 weights;
    DirectX::XMUINT4 boneIndiecs;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[5];
};

const D3D11_INPUT_ELEMENT_DESC VertexPosColor::inputLayout[3] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
    {"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,28,D3D11_INPUT_PER_VERTEX_DATA,0}
};
const D3D11_INPUT_ELEMENT_DESC VertexPosNormalTex::inputLayout[3] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
    {"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,24,D3D11_INPUT_PER_VERTEX_DATA,0}
};
const D3D11_INPUT_ELEMENT_DESC SkinnedVertexIn::inputLayout[5] =
{
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
    {"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,24,D3D11_INPUT_PER_VERTEX_DATA,0},
    { "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT,0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "BONEINDICES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

GameApp::GameApp(HINSTANCE hInstance)
    : D3DApp(hInstance)
{

}

GameApp::~GameApp()
{
}

bool GameApp::Init()
{
    if (!D3DApp::Init())
        return false;

    timer.Start();

    if (!InitEffect())
        return false;

    if (!InitResource())
        return false;

    timer.Reset();
    m_pMouse->SetWindow(m_hMainWnd);
    m_pMouse->SetMode(DirectX::Mouse::MODE_ABSOLUTE);

    return true;
}

bool GameApp::InitEffect()
{
    ComPtr<ID3DBlob> blob;

    //HR(CreateShaderFromFile(nullptr, L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\TriangleVS.hlsl", "VShader", "vs_5_0", blob.ReleaseAndGetAddressOf()));
    //HR(m_pd3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, m_pVertexShader.GetAddressOf()));
    //2
    HR(CreateShaderFromFile(nullptr, L"BoneVS.hlsl", "VShader", "vs_5_0", blob.ReleaseAndGetAddressOf()));
    HR(m_pd3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, m_pVertexShader2.GetAddressOf()));

    HR(CreateShaderFromFile(nullptr, L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\DeBugVS.hlsl", "VShader", "vs_5_0", blob.ReleaseAndGetAddressOf()));
    HR(m_pd3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, m_pDeBugShader.GetAddressOf()));

   HR( m_pd3dDevice->CreateInputLayout(SkinnedVertexIn::inputLayout, ARRAYSIZE(SkinnedVertexIn::inputLayout),
        blob->GetBufferPointer(), blob->GetBufferSize(), m_pVertexLayout.GetAddressOf()));

    //HR(CreateShaderFromFile(nullptr, L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\planePS.hlsl", "main", "ps_5_0", blob.ReleaseAndGetAddressOf()));
    //HR(m_pd3dDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, m_pPixelShader.GetAddressOf()));

    //2
    HR(CreateShaderFromFile(nullptr, L"BonePS.hlsl", "main", "ps_5_0", blob.ReleaseAndGetAddressOf()));
    HR(m_pd3dDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, m_pPixelShader2.GetAddressOf()));
    return true;
}

void GameApp::OnResize()
{
    D3DApp::OnResize();

    //if (m_pCamera != nullptr)
    //{
    //    m_pCamera->SetFrustum(XM_PI / 3, AspectRatio(), 0.5f, 1000.0f);
    //    m_pCamera->SetViewPort(0.0f, 0.0f, (float)m_ClientWidth, (float)m_ClientHeight);
    //    m_CBOnResize.proj = XMMatrixTranspose(m_pCamera->GetProjXM());

    //    D3D11_MAPPED_SUBRESOURCE mappedData;
    //    HR(m_pd3dImmediateContext->Map(m_pConstantBuffers[2].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    //    memcpy_s(mappedData.pData, sizeof(CBChangesOnResize), &m_CBOnResize, sizeof(CBChangesOnResize));
    //    m_pd3dImmediateContext->Unmap(m_pConstantBuffers[2].Get(), 0);
    //}
}

bool GameApp::InitResource()
{

    VertexPosNormalTex vertices2[] =
    {
         XMFLOAT3( - 1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f),XMFLOAT2( 0.0f,  0.0f),
          XMFLOAT3(1.0f, 1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f), XMFLOAT2(1.0f,  1.0f),
          XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f),  XMFLOAT2(1.0f,  0.0f),
          XMFLOAT3(1.0f,  1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f), XMFLOAT2(1.0f,  1.0f),
         XMFLOAT3(-1.0f,  -1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f), XMFLOAT2(0.0f,  0.0f),
        XMFLOAT3(-1.0f, 1.0f, -1.0f),  XMFLOAT3(0.0f,  0.0f, -1.0f), XMFLOAT2(0.0f,  1.0f),

         XMFLOAT3(-1.0f, -1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f), XMFLOAT2(0.0f,  0.0f),
          XMFLOAT3(1.0f, -1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f), XMFLOAT2(1.0f,  0.0f),
          XMFLOAT3(1.0f,  1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f),  XMFLOAT2(1.0f,  1.0f),
          XMFLOAT3(1.0f,  1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f), XMFLOAT2(1.0f,  1.0f),
         XMFLOAT3(-1.0f,  1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f), XMFLOAT2(0.0f,  1.0f),
         XMFLOAT3(-1.0f, -1.0f,  1.0f),  XMFLOAT3(0.0f,  0.0f,  1.0f), XMFLOAT2(0.0f,  0.0f),

         XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f),  XMFLOAT2(1.0f,  0.0f),//5
         XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f),  XMFLOAT2(1.0f,  1.0f),//1
         XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//0
         XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//0
         XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  0.0f),//4
         XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),//5

          XMFLOAT3(1.0f,  1.0f,  1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),//6
          XMFLOAT3(1.0f,  -1.0f, -1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//3
         XMFLOAT3(1.0f, 1.0f, -1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f,  1.0f),//2
         XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//3
          XMFLOAT3(1.0f, 1.0f,  1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),//6
         XMFLOAT3(1.0f,  -1.0f,  1.0f),  XMFLOAT3(1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f,  0.0f),//7

        XMFLOAT3(-1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f, -1.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),
         XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f,  0.0f), XMFLOAT2(1.0f,  1.0f),
         XMFLOAT3(1.0f, -1.0f,  1.0f),  XMFLOAT3(0.0f, -1.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),
         XMFLOAT3(1.0f, -1.0f,  1.0f),  XMFLOAT3(0.0f, -1.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),
         XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT3(0.0f, -1.0f,  0.0f),  XMFLOAT2(0.0f,  0.0f),
         XMFLOAT3(-1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f, -1.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),

         XMFLOAT3(-1.0f,  1.0f, -1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//1
          XMFLOAT3(1.0f,  1.0f, 1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),//6
          XMFLOAT3(1.0f,  1.0f,  -1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(1.0f,  1.0f),//2
         XMFLOAT3(1.0f,  1.0f,  1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(1.0f,  0.0f),//6
        XMFLOAT3(-1.0f,  1.0f, -1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(0.0f,  1.0f),//1
        XMFLOAT3(-1.0f,  1.0f, 1.0f),  XMFLOAT3(0.0f,  1.0f,  0.0f), XMFLOAT2(0.0f,  0.0f)//5
    };

    VertexPosColor vertices[] =
    {
        { XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) ,XMFLOAT2(0.0f,0.0f),},
        { XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) ,XMFLOAT2(0.0f,0.0f),},
        { XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f),XMFLOAT2(0.0f,0.0f), },
        { XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) ,XMFLOAT2(0.0f,0.0f),},
        { XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f),XMFLOAT2(0.0f,0.0f), },
        { XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) ,XMFLOAT2(0.0f,0.0f),},
        { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),XMFLOAT2(0.0f,0.0f), },
        { XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 1.0f, 1.0f),XMFLOAT2(0.0f,0.0f), }
    };


    DWORD indices[] =
    {
        // 正面
0, 1, 2,
2, 3, 0,
// 左面
4, 5, 1,
1, 0, 4,
// 顶面
1, 5, 6,
6, 2, 1,
// 背面
7, 6, 5,
5, 4, 7,
// 右面
3, 2, 6,
6, 7, 3,
// 底面
4, 0, 3,
3, 7, 4
    };

    D3D11_BUFFER_DESC vbd;
    ZeroMemory(&vbd, sizeof(vbd));
    vbd.Usage = D3D11_USAGE_IMMUTABLE;
    vbd.ByteWidth = sizeof(vertices2);
    vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags = 0; 

    D3D11_SUBRESOURCE_DATA InitData;
    ZeroMemory(&InitData, sizeof(InitData));


    InitData.pSysMem = vertices2;
    HR(m_pd3dDevice->CreateBuffer(&vbd, &InitData, m_pVertexBuffer.GetAddressOf()));

    //D3D11_BUFFER_DESC ibd;
    //ZeroMemory(&ibd, sizeof(ibd));
    //ibd.Usage = D3D11_USAGE_IMMUTABLE;
    //ibd.ByteWidth = sizeof (indices);
    //ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    //ibd.CPUAccessFlags = 0;

    //InitData.pSysMem = indices;
    //HR(m_pd3dDevice->CreateBuffer(&ibd, &InitData, m_pindexBuffer.GetAddressOf()));
    //m_pd3dImmediateContext->IASetIndexBuffer(m_pindexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);//绑定到缓冲区

    D3D11_BUFFER_DESC cbd;
    ZeroMemory(&cbd, sizeof(cbd));
    cbd.Usage = D3D11_USAGE_DYNAMIC;
    cbd.ByteWidth = sizeof(CBufferReFreashFrequently);//注意：在创建常量缓冲区时，描述参数ByteWidth必须为16的倍数，因为HLSL的常量缓冲区本身以及对它的读写操作需要严格按16字节对齐
    cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    HR(m_pd3dDevice->CreateBuffer(&cbd, nullptr, m_pConstantBuffer[0].GetAddressOf()));
    cbd.ByteWidth = sizeof(CBufferReFreashRarely);
    HR(m_pd3dDevice->CreateBuffer(&cbd, nullptr, m_pConstantBuffer[1].GetAddressOf()));
    cbd.ByteWidth = sizeof(CBufferReFreshOnResize);
    HR(m_pd3dDevice->CreateBuffer(&cbd, nullptr, m_pConstantBuffer[2].GetAddressOf()));
    cbd.ByteWidth = sizeof(CBufferInstance);
    HR(m_pd3dDevice->CreateBuffer(&cbd, nullptr, m_pConstantBuffer[3].GetAddressOf()));

    wchar_t const* filename = L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\AnimaBakeDataAll.HDR";
    wchar_t const* filenameTranslation = L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\AnimaBakeDataTranslationAll.HDR";
    ScratchImage hdrImage;
    ScratchImage hdrImageTranslation;

    HR(LoadFromHDRFile(filename, nullptr, hdrImage));
    HR(LoadFromHDRFile(filenameTranslation, nullptr, hdrImageTranslation));
    HR(CreateShaderResourceView(m_pd3dDevice.Get(), hdrImage.GetImages(), hdrImage.GetImageCount(), hdrImage.GetMetadata()
        , m_pAnimaTexture.GetAddressOf()));

    HR(CreateShaderResourceView(m_pd3dDevice.Get(), hdrImageTranslation.GetImages(), hdrImageTranslation.GetImageCount(), hdrImageTranslation.GetMetadata()
        , m_pAnimaTextureTranslation.GetAddressOf()));

    //HR(CreateDDSTextureFromFile(m_pd3dDevice.Get(), L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\WoodCrate.dds ", nullptr,m_pWood.GetAddressOf()));
    //HR(CreateWICTextureFromFile(m_pd3dDevice.Get(), L"E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\AnimaBakeData.bmp", nullptr, m_pAnimaTexture.GetAddressOf()));

    D3D11_SAMPLER_DESC samplerDesc;
    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    samplerDesc.MinLOD = 0;
    samplerDesc.MipLODBias = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
    samplerDesc.MaxAnisotropy = 1;

    m_pd3dDevice->CreateSamplerState(&samplerDesc, m_pSamplerState.GetAddressOf());

    g1 = new MeshGeometry(m_pd3dDevice.Get());

    //g1->LoadMeshWithSkinnedAnimation("robot-lod0-animations.x");
    //g1->LoadMeshWithSkinnedAnimation("Breakdance 1990.fbx");
    g1->LoadMeshWithSkinnedAnimation("E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\test3.fbx");
    //g1->LoadMeshWithSkinnedAnimation("E:\\DX_windowSDK_self - backup\\Dx_winSDK_self\\Rumba Dancing.fbx");

    g1->Transforms.resize(g1->m_NumBone);
    g1->BoneTransform(0.0f, g1->Transforms);
    for (uint i = 0; i < g1->m_NumBone; i++)
    {
        m_cBufferFrequently.boneTransform[i] = XMLoadFloat4x4(&g1->Transforms[i]);
    }

    //camera setting
    m_cameraMode = CameraMode::FirstPerson;
    auto camera = std::shared_ptr<FirstPersonCamera>(new FirstPersonCamera);
    m_pCamera = camera;
    camera->SetViewPort(0.0f, 0.0f, (float)m_ClientWidth, (float)m_ClientHeight);
    camera->LookAt(XMFLOAT3(), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));

    m_pCamera->SetFrustum(XM_PI / 4, AspectRatio(), 0.5f, 1000.0f);
    m_cBufferResize.proj = XMMatrixTranspose(m_pCamera->GetProjXM());


    m_cBufferFrequently.world = XMMatrixIdentity();
    m_cBufferFrequently.view = XMMatrixTranspose(XMMatrixLookAtLH(
        XMVectorSet(0.0f, 0.0f, -5.0f, 0.0f),
        XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
        XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)
    ));
    m_cBufferResize.proj = XMMatrixTranspose(XMMatrixPerspectiveFovLH(XM_PIDIV2, AspectRatio(), 1.0f, 1000.0f));
    m_cBufferFrequently.worldInvTranspose = XMMatrixIdentity();
    m_dirLit.direction = XMFLOAT3(0.0f, 0.5f, -0.5f);

    m_cBufferRarely.dirlit = m_dirLit;
    m_cBufferRarely.g_InstanceMatricesWidth.x = 1024;
    //Create Instance Data
    float scaleX = 5.0f;
    float scaleZ = 0.5f;
    float perOffset =0.7f;


    //BoundingFrustum frustum;
    //BoundingFrustum::CreateFromMatrix(frustum, m_pCamera->GetProjXM());
    //XMMATRIX viewMat = m_pCamera->GetViewProjXM();

    //BoundingOrientedBox localOrientedBox, orientedBox;
    //BoundingOrientedBox::CreateFromBoundingBox(localOrientedBox, boundingBox);
    m_batch.reset();
    m_instancePos.resize(INSTANCE_AMOUNT);
    m_instanceData.resize(INSTANCE_AMOUNT);
    for(int i=0;i<10;i++)
    {
        for (int j = 0; j < 10; j++)
        {
            InstanceDataElelment tempData;
            XMMATRIX m = {
                    1.0f, 0.0f, 0.0f, (j)*perOffset* scaleX,
                    0.0f, 1.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, 1.0f, (i * 10)* perOffset* scaleZ,
                    0.0f,0.0f,0.0f,1.0f,
            };
            XMFLOAT4X4 temp4x4;
            //m = XMMatrixTranspose(m);
            XMStoreFloat4x4(&temp4x4,m);
            XMFLOAT4 row1 = XMFLOAT4(temp4x4._11, temp4x4._12, temp4x4._13, temp4x4._14);
            XMFLOAT4 row2 = XMFLOAT4(temp4x4._21, temp4x4._22, temp4x4._23, temp4x4._24);           
            XMFLOAT4 row3 = XMFLOAT4(temp4x4._31, temp4x4._32, temp4x4._33, temp4x4._34);

            tempData.world1 = row1;
            tempData.world2 = row2;
            tempData.world3 = row3;

            int tempID = rand();

            m_instanceData[i*10+j]=tempData;
            m_instanceData[i * 10 + j].indexAndOffset.x = 0; //All people will use Animation1
            m_instanceData[i * 10 + j].indexAndOffset.y = 0; 
            m_instanceData[i * 10 + j].animationIndexAndEndOffset.x = tempID % 2 == 0 ? 0 : 107;
            m_instanceData[i * 10 + j].animationIndexAndEndOffset.y = tempID %2==0?105:212;
            m_instanceData[i * 10 + j].color = tempID % 2 == 0 ? XMFLOAT4(0.2f, 0.4f, 0.8f, 1.0f) : XMFLOAT4(0.2f, 0.8f, 0.1f, 1.0f);
            m_instancePos[i * 10 + j].SetPosition((j)*perOffset * scaleX, 0.0f, (i * 10) * perOffset * scaleZ);
        }
    }
    for (int i = 0; i < m_instanceData.size(); i++)
    {
        m_cBufferInstance.instancePos[i] = m_instanceData[i];
    }

    testCameraFar = 100.0f;
    auto cameraDebug = new FirstPersonCamera;
    cameraDebug->SetViewPort(0.0f, 0.0f, 128, 72);
    cameraDebug->LookAt(XMFLOAT3(0.0f, 0.0f, -10.0f), XMFLOAT3(0.0f, 0.0f, 1000.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
    cameraDebug->SetFrustum(XM_PI / 2.0f, AspectRatio(), 0.5f, testCameraFar);
    m_pCameraDeBug = cameraDebug;

    BoundingBox boundingBox = g1->maxBBox;
    boundingBox.Extents = XMFLOAT3(boundingBox.Extents.x / 100.0f, boundingBox.Extents.y / 100.0f, boundingBox.Extents.z / 50.0f);
    boundingBox.Center = XMFLOAT3(boundingBox.Center.x / 100.0f, boundingBox.Center.y / 100.0f, boundingBox.Center.z / 50.0f);
    BoundingOrientedBox localOrientedBox, orientedBox;
    BoundingFrustum frustum;
    BoundingFrustum::CreateFromMatrix(frustum, m_pCamera->GetProjXM());
    BoundingOrientedBox::CreateFromBoundingBox(localOrientedBox, boundingBox);
    XMMATRIX V = m_pCamera->GetViewXM();

    for (int i = 0; i < INSTANCE_AMOUNT; i++)
    {
        localOrientedBox.Transform(orientedBox, m_instancePos[i].GetLocalToWorldMatrixXM() * V);
        m_bboxList.push_back(orientedBox);
        //if (frustum.Intersects(orientedBox))
        //{
        //    m_AcceptedData.push_back(m_instancePos[i]);
        //    m_AcceptedIndices.push_back(i);
        //}
    }

    

    //set bake parameter
    needBake =false;
    starOffset = 0;
    perRowBoneAmount = 52;
    animaFramesCount = 105;
    FPS = 30;
    maxValue = 0;
    //bake image
    if (needBake)
    {
        m_pixel = new float[IMAGE_HEIGHT * IMAGE_WIDTH * 16];
        m_pixelTraslation = new float[IMAGE_HEIGHT * IMAGE_WIDTH * 16];
        timeCount = (float)animaFramesCount / (float)FPS;
        currentFrames = starOffset;
    }

    //m_pd3dImmediateContext->VSSetConstantBuffers(3, 1, m_pConstantBuffer[3].GetAddressOf());
    m_pd3dImmediateContext->VSSetShaderResources(0, 1, m_pAnimaTexture.GetAddressOf());
    m_pd3dImmediateContext->VSSetShaderResources(1, 1, m_pAnimaTextureTranslation.GetAddressOf());

    D3D11_MAPPED_SUBRESOURCE mappedData;
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[0].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferReFreashFrequently), &m_cBufferFrequently, sizeof(CBufferReFreashFrequently));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[0].Get(), 0);

    ZeroMemory(&mappedData, sizeof(mappedData));
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[1].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferReFreashRarely), &m_cBufferRarely, sizeof(CBufferReFreashRarely));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[1].Get(), 0);


    ZeroMemory(&mappedData, sizeof(mappedData));
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[2].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferReFreshOnResize), &m_cBufferResize, sizeof(CBufferReFreshOnResize));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[2].Get(), 0);

    ZeroMemory(&mappedData, sizeof(mappedData));
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[3].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferInstance), &m_cBufferInstance, sizeof(CBufferInstance));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[3].Get(), 0);

    m_batch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(m_pd3dImmediateContext.Get());
    return true;
}

void GameApp::UpdateScene(float dt)
{
    timer.Tick();

    static float cubePhi = 0.0f, cubeTheta = 0.0f;
    Mouse::State mouseState = m_pMouse->GetState();
    Mouse::State lastMouseState = m_MouseTracker.GetLastState();

    Keyboard::State keyState = m_pKeyboard->GetState();
    Keyboard::State lastKeyState = m_KeyboardTracker.GetLastState();
    auto cam1st = std::dynamic_pointer_cast<FirstPersonCamera>(m_pCamera);

    m_MouseTracker.Update(mouseState);
    m_KeyboardTracker.Update(keyState);

    if (mouseState.leftButton == true && m_MouseTracker.leftButton == m_MouseTracker.HELD)
    {
        cubeTheta -= (mouseState.x - lastMouseState.x) * 0.01f;
        cubePhi -= (mouseState.y - lastMouseState.y) * 0.01f;
        
    }
    
    float d1 = 0.0f, d2 = 0.0f;

if (keyState.IsKeyDown(Keyboard::W))
{
    d1 += dt;
}

if (keyState.IsKeyDown(Keyboard::S))
{
    d1 -= dt;
}
if (keyState.IsKeyDown(Keyboard::A))
{
    d2 -= dt;
}
if (keyState.IsKeyDown(Keyboard::D))
{
    d2 += dt;
}

if (!isStart&& needBake)
{
    timer.Reset();
}
    float runningTime = timer.TotalTime();

    double perFrameTime = 1.0f / FPS;
    currentFrames = runningTime / perFrameTime;
    if (needBake && !isFinshed && animaFramesCount > 0)
    {
        isStart = true;

        if (runningTime < timeCount && currentFrames < starOffset + animaFramesCount)
        {
                for (int j = 0; j < perRowBoneAmount * 12;)
                {
                    int boneIndex = j / 12;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j] = g1->Transforms[boneIndex]._11;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 1] = g1->Transforms[boneIndex]._21;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 2] = g1->Transforms[boneIndex]._31;

                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 3] = g1->Transforms[boneIndex]._12;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 4] = g1->Transforms[boneIndex]._22;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 5] = g1->Transforms[boneIndex]._32;

                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 6] = g1->Transforms[boneIndex]._13;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 7] = g1->Transforms[boneIndex]._23;
                    m_pixel[currentFrames * IMAGE_WIDTH * 3 + j + 8] = g1->Transforms[boneIndex]._33;

                    m_pixelTraslation[currentFrames * IMAGE_WIDTH * 3 + j + 9] = g1->Transforms[boneIndex]._14;
                    m_pixelTraslation[currentFrames * IMAGE_WIDTH * 3 + j + 10] = g1->Transforms[boneIndex]._24;
                    m_pixelTraslation[currentFrames * IMAGE_WIDTH * 3 + j + 11] = g1->Transforms[boneIndex]._34;

                    j += 12;
                }
        }
        else
        {
            isFinshed = true;
            for (uint i = 0; i < IMAGE_WIDTH * IMAGE_HEIGHT * 3; i++)
            {
                m_pixel[i] = (m_pixel[i] + 20.0f)/20.0f;
                m_pixelTraslation[i] = (m_pixelTraslation[i]+2000) / 4000.0f;
            } 
            const char* fileName = "AnimaBakeData2.hdr";
            //stbi_write_hdr(fileName, IMAGE_WIDTH, IMAGE_HEIGHT, 3, m_pixel);

            const char* fileNameTranslation = "AnimaBakeDataTranslation2.hdr";
            //stbi_write_hdr(fileNameTranslation, IMAGE_WIDTH, IMAGE_HEIGHT, 3, m_pixelTraslation);
        }
    }

    //calculate bone animation
    //g1->BoneTransform(runningTime, g1->Transforms);
    //for (uint i = 0; i < g1->m_NumBone; i++)
    //{
    //    m_cBufferFrequently.boneTransform[i] = XMLoadFloat4x4(&g1->Transforms[i]);
    //}

    //for (int i = 0; i < m_instanceData.size(); i++)
    //{
    //    m_cBufferInstance.instancePos[i].indexAndOffset.y = currentFrames% (m_instanceData[i].animationIndexAndEndOffset.y - m_instanceData[i].indexAndOffset.x);
    //}

    float d3 = 0;
    if (keyState.IsKeyDown(Keyboard::Left))
    {
        d3 -= dt;
    }
    if (keyState.IsKeyDown(Keyboard::Right))
    {
        d3 += dt;
    }
    if (keyState.IsKeyDown(Keyboard::Down))
    {
        testCameraFar -= dt*70;
        if (testCameraFar < 1.0f)
        {
            testCameraFar = 1.0f;
        }
    }
    if (keyState.IsKeyDown(Keyboard::Up))
    {
        testCameraFar += dt * 70;
        if (testCameraFar > 2000.0f)
        {
            testCameraFar = 2000.0f;
        }
    }



    m_pCameraDeBug->Strafe(d3 * 30.0f);
    m_AcceptedData.clear();
    m_AcceptedIndices.clear();
    m_AcceptedData.clear();
    m_AcceptedData.resize(0);
    m_AcceptedIndices.resize(0);
    BoundingBox boundingBox = g1->maxBBox;
    boundingBox.Extents = XMFLOAT3(boundingBox.Extents.x / 100.0f, boundingBox.Extents.y / 100.0f, boundingBox.Extents.z / 50.0f);
    boundingBox.Center = XMFLOAT3(boundingBox.Center.x / 100.0f, boundingBox.Center.y / 100.0f, boundingBox.Center.z / 50.0f);
    BoundingOrientedBox localOrientedBox, orientedBox;

    BoundingFrustum::CreateFromMatrix(m_frustum, m_pCameraDeBug->GetProjXM());
   
    m_frustum.Far = testCameraFar;
    BoundingOrientedBox::CreateFromBoundingBox(localOrientedBox, boundingBox);
    XMMATRIX V = m_pCameraDeBug->GetViewXM();
    for (int i = 0; i < INSTANCE_AMOUNT; i++)
    {
        XMMATRIX m = m_instancePos[i].GetLocalToWorldMatrixXM();
        localOrientedBox.Transform(orientedBox, m_instancePos[i].GetLocalToWorldMatrixXM() * V);
        if (m_frustum.Intersects(orientedBox))
        {
            m_AcceptedData.push_back(m_instancePos[i]);      
            m_AcceptedIndices.push_back(i);
        }
    }
    m_frustum.Origin = m_pCameraDeBug->GetPosition();
    
    cam1st->Walk(d1 * 6.0f);
    cam1st->Strafe(d2 * 6.0f);
    m_cBufferFrequently.view = XMMatrixTranspose(cam1st->GetViewXM());
    m_cBufferFrequently.world = XMMatrixTranspose(XMMatrixRotationY(cubeTheta) * XMMatrixRotationX(cubePhi));
    m_pCamera = cam1st;

    for (int i = 0; i < m_AcceptedData.size(); i++)
    {
        m_cBufferInstance.instancePos[i].indexAndOffset.y = currentFrames % (m_instanceData[m_AcceptedIndices[i]].animationIndexAndEndOffset.y - m_instanceData[m_AcceptedIndices[i]].animationIndexAndEndOffset.x)
            + m_instanceData[m_AcceptedIndices[i]].animationIndexAndEndOffset.x;
        m_cBufferInstance.instancePos[i].world1 = XMFLOAT4(1.0f, 0.0f, 0.0f, m_AcceptedData[i].GetPosition().x);
        m_cBufferInstance.instancePos[i].world2 = XMFLOAT4(1.0f, 0.0f, 0.0f, m_AcceptedData[i].GetPosition().y);
        m_cBufferInstance.instancePos[i].world3 = XMFLOAT4(1.0f, 0.0f, 0.0f, m_AcceptedData[i].GetPosition().z);

    }

    D3D11_MAPPED_SUBRESOURCE mappedData;
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[0].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferReFreashFrequently), &m_cBufferFrequently, sizeof(CBufferReFreashFrequently));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[0].Get(), 0);

    ZeroMemory(&mappedData, sizeof(mappedData));
    HR(m_pd3dImmediateContext->Map(m_pConstantBuffer[3].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));
    memcpy_s(mappedData.pData, sizeof(CBufferInstance), &m_cBufferInstance, sizeof(CBufferInstance));
    m_pd3dImmediateContext->Unmap(m_pConstantBuffer[3].Get(), 0);
}

void GameApp::DrawScene()
{
    assert(m_pd3dImmediateContext);
    assert(m_pSwapChain);

    static float black[4] = { 0.0f, 0.0f, 0.0f, 1.0f };	// RGBA = (0,0,0,255)
    m_pd3dImmediateContext->ClearRenderTargetView(m_pRenderTargetView.Get(), black);
    m_pd3dImmediateContext->ClearDepthStencilView(m_pDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

    g1->DrawAllMesh(m_pd3dImmediateContext.Get(), m_pVertexLayout.Get(), m_pVertexShader2.Get(), m_pPixelShader2.Get(), m_AcceptedIndices.size(),
        m_pConstantBuffer[0].GetAddressOf(), m_pConstantBuffer[1].GetAddressOf(), m_pConstantBuffer[2].GetAddressOf(),
        m_pConstantBuffer[3].GetAddressOf());

    //Debug setting
    //m_pd3dImmediateContext->VSSetShader(m_pDeBugShader.Get(), nullptr, 0);
    //m_batch->Begin();
    //DX::Draw(m_batch.get(), m_frustum, Colors::Blue);
    //for (int i = 0; i < m_bboxList.size(); i++)
    //{
    //    DX::Draw(m_batch.get(), m_bboxList[i], Colors::Blue);
    //}

    //m_batch->End();

    HR(m_pSwapChain->Present(0, 0));

}
