#include "Triangle.hlsli"
#define ANIMA_WIDTH 512
SamplerState g_samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};

float4x4 loadBoneMatrix(uint4 animationData, uint bone)
{
    float4x4 rval = g_Identity;

    uint baseIndex = (animationData.x + animationData.y) * ANIMA_WIDTH;
    baseIndex += (4 * bone);

    uint baseU = baseIndex % ANIMA_WIDTH;
    uint baseV = baseIndex / ANIMA_WIDTH;

    float3 mat1 = (gTex.Load(uint3(baseU, baseV, 0)).rgb * 20) - 20.0f;
    float3 mat2 = (gTex.Load(uint3(baseU + 1, baseV, 0)).rgb * 20) - 20.0f;
    float3 mat3 = (gTex.Load(uint3(baseU + 2, baseV, 0)).rgb * 20) - 20.0f;
    float3 mat4 = (gTexTranslation.Load(uint3(baseU + 3, baseV, 0)).rgb * 4000.0f) - 2000.0f;



    rval = float4x4(mat1.x, mat1.y, mat1.z, 0.0f,
        mat2.x, mat2.y, mat2.z, 0.0f,
        mat3.x, mat3.y, mat3.z, 0.0f,
        mat4.x, mat4.y, mat4.z, 1.0f);

    //rval = float4x4(mat1.x, 0.0f, 0.0f, 0.0f,
    //    0.0f, mat2.y, 0.0f, 0.0f,
    //    0.0f, 0.0f, mat3.z, 0.0f,
    //    0.0f, 0.0f, 0.0f, 1.0f);


    return rval;
}

VertexPosHWNormalTex VShader(SkinnedVertexIn v)
{
    uint4 animationData = g_instance[v.instanceId].animationData;
    float4 worldMatrix1 = g_instance[v.instanceId].world1;
    float4 worldMatrix2 = g_instance[v.instanceId].world2;
    float4 worldMatrix3 = g_instance[v.instanceId].world3;
    //float4 instancColor = g_instance[v.instanceId].color;

    //animationData.x = 0;
    //animationData.y = 0;

    float3 normalL = float3(0.0f, 0.0f, 0.0f);
    matrix Trasnform = loadBoneMatrix(animationData, v.boneIndiecs.x) * v.weights.x;

    if (v.weights.y > 0)
    {
        Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.y) * v.weights.y;
        if (v.weights.z > 0)
        {
            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.z) * v.weights.z;
        }
        if (v.weights.w > 0)
        {
            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.w) * v.weights.w;
        }
    }
    normalL += mul(v.normal, (float3x3)Trasnform);
    //matrix Trasnform = boneTransform[v.boneIndiecs[0]] * v.weights.x;
    //Trasnform += boneTransform[v.boneIndiecs[1]] * v.weights.y;
    //Trasnform += boneTransform[v.boneIndiecs[2]] * v.weights.z;
    //Trasnform += boneTransform[v.boneIndiecs[3]] * v.weights.w;

    VertexPosHWNormalTex o;
    o.pos = mul(float4(v.pos, 1.0f), Trasnform);
    // o.pos = mul(float4(v.pos, 1.0f), world);



    float4x4 instanceTransform = float4x4(1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        worldMatrix1.w, 0.0f, worldMatrix3.w, 1.0f);


    float4x4 scale = float4x4(0.001f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.001f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.001f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f);

    o.pos = mul(o.pos, scale);
    o.pos = mul(o.pos, instanceTransform);

    o.pos = mul(o.pos, world);
    o.pos = mul(o.pos, view);
    o.pos = mul(o.pos, proj);

    //// o.normalW = mul(v.normal, (float3x3)worldInvTranspose);


    o.color = g_instance[v.instanceId].color;
    o.posW = mul(float4(v.pos, 1.0f), world);
    o.normalW = mul(normalL, (float3x3)worldInvTranspose);
    o.uv = v.uv;
    return o;
}
//float4x4 loadBoneMatrix(uint4 animationData, uint bone)
//{
//    float4x4 rval = g_Identity;
//
//    uint baseIndex = (animationData.x + animationData.y) * ANIMA_WIDTH;
//    baseIndex += (4 * bone);
//
//    uint baseU = baseIndex % ANIMA_WIDTH;
//    uint baseV = baseIndex / ANIMA_WIDTH;
//
//    float3 mat1 = (gTex.Load(uint3(baseU, baseV, 0)).rgb * 20) - 20.0f;
//    float3 mat2 = (gTex.Load(uint3(baseU + 1, baseV, 0)).rgb * 20) - 20.0f;
//    float3 mat3 = (gTex.Load(uint3(baseU + 2, baseV, 0)).rgb * 20) - 20.0f;
//    float3 mat4 = (gTexTranslation.Load(uint3(baseU + 3, baseV, 0)).rgb * 4000.0f) - 2000.0f;
//
//
//
//    rval = float4x4(mat1.x, mat1.y, mat1.z, 0.0f,
//        mat2.x, mat2.y, mat2.z, 0.0f,
//        mat3.x, mat3.y, mat3.z, 0.0f,
//        mat4.x, mat4.y, mat4.z, 1.0f);
//
//    //rval = float4x4(mat1.x, 0.0f, 0.0f, 0.0f,
//    //    0.0f, mat2.y, 0.0f, 0.0f,
//    //    0.0f, 0.0f, mat3.z, 0.0f,
//    //    0.0f, 0.0f, 0.0f, 1.0f);
//
//
//    return rval;
//}
//
//VertexPosHWNormalTex VShader(SkinnedVertexIn v)
//{
//    uint4 animationData = g_instance[v.instanceId].animationData;
//    float4 worldMatrix1 = g_instance[v.instanceId].world1;
//    float4 worldMatrix2 = g_instance[v.instanceId].world2;
//    float4 worldMatrix3 = g_instance[v.instanceId].world3;
//    //float4 instancColor = g_instance[v.instanceId].color;
//
//    //animationData.x = 0;
//    //animationData.y = 0;
//
//    float3 normalL = float3(0.0f, 0.0f, 0.0f);
//    matrix Trasnform = loadBoneMatrix(animationData, v.boneIndiecs.x) * v.weights.x;
//
//    if (v.weights.y > 0)
//    {
//        Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.y) * v.weights.y;
//        if (v.weights.z > 0)
//        {
//            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.z) * v.weights.z;
//        }
//        if (v.weights.w > 0)
//        {
//            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.w) * v.weights.w;
//        }
//    }
//    normalL += mul(v.normal, (float3x3)Trasnform);
//    //matrix Trasnform = boneTransform[v.boneIndiecs[0]] * v.weights.x;
//    //Trasnform += boneTransform[v.boneIndiecs[1]] * v.weights.y;
//    //Trasnform += boneTransform[v.boneIndiecs[2]] * v.weights.z;
//    //Trasnform += boneTransform[v.boneIndiecs[3]] * v.weights.w;
//
//    VertexPosHWNormalTex o;
//    o.pos = mul(float4(v.pos, 1.0f), Trasnform);
//    // o.pos = mul(float4(v.pos, 1.0f), world);
//
//    float4x4 instanceTransform = float4x4(1.0f, 0.0f, 0.0f, 0.0f,
//        0.0f, 1.0f, 0.0f, 0.0f,
//        0.0f, 0.0f, 1.0f, 0.0f,
//        worldMatrix1.w, 0.0f, worldMatrix3.w, 1.0f);
//
//
//    float4x4 scale = float4x4(0.001f, 0.0f, 0.0f, 0.0f,
//        0.0f, 0.001f, 0.0f, 0.0f,
//        0.0f, 0.0f, 0.001f, 0.0f,
//        0.0f, 0.0f, 0.0f, 1.0f);
//
//    o.pos = mul(o.pos, scale);
//    o.pos = mul(o.pos, instanceTransform);
//
//    o.pos = mul(o.pos, world);
//    o.pos = mul(o.pos, view);
//    o.pos = mul(o.pos, proj);
//
//    //// o.normalW = mul(v.normal, (float3x3)worldInvTranspose);
//
//
//
//    o.posW = mul(float4(v.pos, 1.0f), world);
//    o.normalW = mul(normalL, (float3x3)worldInvTranspose);
//    o.uv = v.uv;
//    return o;
//}