#include "Triangle.hlsli"


float4 main(VertexPosHWNormalTex i) :SV_Target
{
    
    //float3 normalW = float3(0.0f,0.0f,1.0f);
    float4 texColor = gTex.Sample(gSampler, i.uv);
    float3 normal = normalize(i.normalW);
    float3 lightDir = float3(0.2,-1.0f,0.2f);
    float diff = max(0,dot(normal, -lightDir))*0.5f+0.5f;


    float3 finalColor = diff *  texColor.rgb;
    

    return float4(finalColor,1.0f);
}